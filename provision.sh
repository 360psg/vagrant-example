#!/usr/bin/env bash

apt-get update
apt-get install -y apache2 libapache2-mod-php5 php5
rm -rf /var/www
ln -fs /vagrant/public /var/www
service apache2 restart

