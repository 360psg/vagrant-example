Vagrant Example
===============

- Install VirtualBox & [Vagrant](http://downloads.vagrantup.com/tags/v1.2.2) on your computer.
- Clone this project
- Cd into your clone, and type `vagrant up`
- Open your browser to http://localhost:8080/

